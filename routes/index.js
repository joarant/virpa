var express = require('express');
var router = express.Router();
var getRawBody = require('raw-body');
const createCsvWriter = require('csv-writer').createArrayCsvWriter;
var fs = require('fs');

var date = new Date();
var getRawBody = require('raw-body');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Virpa' });
});


router.use(function (req, res, next) {
  if (req.headers['content-type'] === 'application/octet-stream') {
      getRawBody(req, {
          length: req.headers['content-length'],
          encoding: req.charset
      }, function (err, string) {
          if (err)
              return next(err);
          req.body = string;
          next();
       })
  }
  else {
      next();
  }
});

router.put('/', function(req,res){
  'use strict';
    var time = ""+date.getFullYear() + date.getMonth()+date.getDate()+date.getHours()+date.getMinutes()+date.getSeconds();
    saveToFile(decodeURIComponent(req.body),"./recivedFiles/"+time+".txt");
    res.sendStatus(200);
  });
  
  
  function saveToFile(data,saveLocation){
    fs.writeFileSync(saveLocation, data, function(err) {
      if(err) {
          return console.log(err);
      }
      else{
        return console.log("The file was saved!");
      }
    });
  }

module.exports = router;
